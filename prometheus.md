## 3. session: Monitoring: Prometheus

### Prometheus bemutatás

Prometheus egy nyílt forráskódú rendszer a mérési és figyelési adatok gyűjtésére és rögzítésére. Alapvetően használják a mikroszolgáltatások és konténerek környezetében, de számos más alkalmazásban is. Prometheus lehetővé teszi a rendszer teljesítményének és állapotának figyelését, és lehetővé teszi a problémák előrejelzését.

### Prometheus kubernetes setup

Create namespace (for logical separation)
```commandline
kubectl create namespace prometheus-namespace
```

Create a persistent volume 


Create a file name 'prometheus-k8s-storage.yaml' with the following content
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: prometheus-data-pvc
  namespace: prometheus-namespace
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 2Gi
```

Deploy PVC
```commandline
kubectl apply -f ./prometheus-k8s-storage.yaml
```

Create a file name prometheus-k8s.yaml
```yaml
apiVersion: v1
data:
  prometheus.yml: |
    global:
      scrape_interval: 30s 
      scrape_timeout: 25s

    scrape_configs:
      - job_name: "prometheus"
        static_configs:
          - targets: ["localhost:9090"]

kind: ConfigMap
metadata:
  namespace: prometheus-namespace
  name: prometheus-configmap
---
apiVersion: v1
kind: Service
metadata:
  name: prometheus-service
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: "http"
  namespace: prometheus-namespace
  labels:
    app: prometheus
spec:
  type: LoadBalancer
  selector:
    app: prometheus
  ports:
    - protocol: TCP
      port: 9090
      targetPort: 9090
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: prometheus
  name: prometheus-deploy
  namespace: prometheus-namespace
spec:
  replicas: 1
  selector:
    matchLabels:
      app: prometheus
  template:
    metadata:
      labels:
        app: prometheus
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/arch
                operator: In
                values:
                - amd64
                - arm64
      containers:
        - name: prometheus-container
          image: prom/prometheus:latest
          ports:
            - containerPort: 9090
          volumeMounts:
            - name: prometheus-config-file
              mountPath: /etc/prometheus
            - name: prometheus-data-storage
              mountPath: /prometheus
      volumes:
        - name: prometheus-config-file
          configMap:
            name: prometheus-configmap
        - name: prometheus-data-storage
          persistentVolumeClaim:
            claimName: prometheus-data-pvc
```

Deploy 
```commandline
kubectl apply -f prometheus-k8s.yaml
```


Check if running
```commandline
kubectl get pods -n prometheus-namespace
```

Check result on: http://localhost:9090

## Prometheus UI bemutatás

A Prometheus UI egy webes felhasználói felület, amely lehetővé teszi a felhasználók számára a Prometheus által gyűjtött adatok megtekintését és vizualizálását. Ezen a felületen lehetőség van az adatok keresésére, grafikonok és diagramok készítésére, valamint a rendszer teljesítményének és állapotának ellenőrzésére.

### Alapvető Prometheus UI funkciók

A Prometheus UI számos hasznos funkciót kínál a felhasználók számára:

1. **Querying (Lekérdezés):** A felhasználók képesek keresni és lekérdezni az összegyűjtött metrikákat a Prometheus Query Language (PromQL) használatával. A lekérdezések lehetővé teszik az adatok összevonását és szűrését.

2. **Grafikonok és Diagramok:** Az UI lehetővé teszi grafikonok és diagramok létrehozását a lekérdezett adatok alapján. Ez segítséget nyújt az adatok vizualizálásához és értelmezéséhez.

3. **Metrikák és Címkék: A Prometheus UI megjeleníti a rendszer metrikáit és azok címkéit. Ez lehetővé teszi az adatok pontos azonosítását és kategorizálását.

4. **Állapotok megjelenítése:** A rendszerállapotok (például a klaszter állapota, szolgáltatások elérhetősége) megjelenítésére szolgáló panel lehetővé teszi a rendszer gyors állapotellenőrzését.

5. **Exportálás és Megosztás:** A felhasználók képesek exportálni és megosztani a létrehozott grafikonokat és lekérdezéseket másokkal.

### Használati útmutató

1. **Adatok lekérdezése:** A bal felső sarokban található "Expression" mezőbe írd be a PromQL lekérdezést. Például: `http_requests_total{job="webserver"}`.

2. **Lekérdezés végrehajtása:** Nyomd meg a "Execute" gombot a lekérdezés végrehajtásához.

3. **Grafikon létrehozása:** A lekérdezés eredményei alapján kattints a "Graph" fülre, majd válassz ki kívánt beállításokat, például az időintervallumot és az aggregálási módokat.

4. **Egyéb lehetőségek:** Az UI tartalmaz további funkciókat, például a "Status" fülön keresztül a rendszerállapot ellenőrzését.

5. **Exportálás és Megosztás:** Ha létrehoztál egy grafikont vagy diagramot, használd az exportálási és megosztási lehetőségeket.

Az Prometheus UI lehetővé teszi a Prometheus rendszer által gyűjtött adatok könnyű megjelenítését és elemzését. Ezáltal a felhasználók képesek gyorsan észlelni és értékelni a rendszerállapot változásait és a teljesítményt.
