from flask import Flask, jsonify, request

app = Flask(__name__)

# Sample data (tasks)
tasks = []

@app.route('/hello')
def get_hello():
    return "hello"

# Route to get all tasks
@app.route('/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

# Route to create a new task
@app.route('/tasks', methods=['POST'])
def create_task():
    task = request.json
    tasks.append(task)
    return jsonify({'message': 'Task created successfully'})

# Route to get a specific task by ID
@app.route('/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    if 0 <= task_id < len(tasks):
        return jsonify({'task': tasks[task_id]})
    return jsonify({'message': 'Task not found'}), 404

# Route to update a specific task by ID
@app.route('/tasks/<int:task_id>', methods=['PUT'])
def update_task(task_id):
    if 0 <= task_id < len(tasks):
        updated_task = request.json
        tasks[task_id] = updated_task
        return jsonify({'message': 'Task updated successfully'})
    return jsonify({'message': 'Task not found'}), 404

# Route to delete a specific task by ID
@app.route('/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    if 0 <= task_id < len(tasks):
        del tasks[task_id]
        return jsonify({'message': 'Task deleted successfully'})
    return jsonify({'message': 'Task not found'}), 404

if __name__ == '__main__':
    app.run(debug=True)