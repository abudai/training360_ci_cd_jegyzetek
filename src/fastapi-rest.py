from fastapi import FastAPI
from pydantic import BaseModel

# uvicorn main:app --reload
app = FastAPI()

# Sample data (tasks)
tasks = []

# Task model using Pydantic
class Task(BaseModel):
    name: str
    description: str
    priority: str

# Route to create a new task
@app.post("/tasks/", response_model=Task)
def create_task(task: Task):
    tasks.append(task)
    return task

# Route to get all tasks
@app.get("/tasks/", response_model=list[Task])
def get_tasks():
    return tasks

# Route to get a specific task by ID
@app.get("/tasks/{task_id}", response_model=Task)
def get_task(task_id: int):
    if 0 <= task_id < len(tasks):
        return tasks[task_id]
    raise HTTPException(status_code=404, detail="Task not found")

# Route to update a specific task by ID
@app.put("/tasks/{task_id}", response_model=Task)
def update_task(task_id: int, updated_task: Task):
    if 0 <= task_id < len(tasks):
        tasks[task_id] = updated_task
        return updated_task
    raise HTTPException(status_code=404, detail="Task not found")

# Route to delete a specific task by ID
@app.delete("/tasks/{task_id}", response_model=Task)
def delete_task(task_id: int):
    if 0 <= task_id < len(tasks):
        deleted_task = tasks.pop(task_id)
        return deleted_task
    raise HTTPException(status_code=404, detail="Task not found")