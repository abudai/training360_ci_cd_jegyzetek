# Git gyakorlat


## Clone es push

1. Clone
   ```shell
   git clone [repository_url]
   cd [repository_name]
   ```
   
2. Add readme
   ```shell
   echo "# My Project" > README.md
   ```

3. Stage es commit:
   ```shell
   git add README.md
   git commit -m "Add README"
   ```
   
4. push
   ```shell
   git push origin main
   ```

## Branching es PR

1. Uj branch letrehozasa
   ```shell
   git branch test
   ```
   
2. Lepjunk at az uj branchre
   ```shell
   git checkout test
   ```

3. Commit
   ```shell
   # Make code changes
   git add .
   git commit -m "Add feature XYZ"
   ```
   
4. push
   ```shell
   git push origin test
   ```
   
5. PR megtekintese letrehozasa es megtekintese



## Pre commit hook

1. `pre-commit` file letrehozasa a .git/hooks mappa alatt. Ha nincs letre kell hozni
2. Kapjon futasi jogot:
   ```bash
   chmod +x .git/hooks/pre-commit
   ```
3. pre-commit fileba:
```bash
   #!/bin/bash

   # Redirect stdout and stderr to a temporary file
   tmpfile=$(mktemp)
   exec 3>&1
   exec > $tmpfile 2>&1

   # Run PEP8 and MyPy checks on all .py files
   git diff --cached --name-only --diff-filter=ACMRTUXB | grep '\.py$' | xargs -r -n1 pep8 --ignore=E501,W503 && git diff --cached --name-only --diff-filter=ACMRTUXB | grep '\.py$' | xargs -r -n1 mypy

   # Restore stdout and stderr
   exec 1>&3 2>&1

   # Check if there were any errors or warnings
   if [ -s $tmpfile ]; then
       echo "Pre-commit checks failed. Please fix the following issues before committing:"
       cat $tmpfile
       rm $tmpfile
       exit 1
   else
       rm $tmpfile
       exit 0
   fi
   ```
4. Legyenek installalva a fuggosek:
```bash
pip install pep8 mypy
```


## Branching strategies:

| Branching Strategy | Description                                       | Use Cases                                                | Pros                                | Cons                               |
|--------------------|---------------------------------------------------|----------------------------------------------------------|------------------------------------|-----------------------------------|
| Feature Branches   | Each feature or task has its branch             | Collaborative development, isolating features           | Isolation, parallel development     | Can lead to a high number of branches, merging challenges |
| Gitflow            | Specific branches for features, releases, etc.  | Well-structured releases, feature segregation           | Clear process, versioned releases   | Complex for small projects, extra overhead                  |
| GitHub Flow        | Mainly uses master branch for everything         | Simplified, continuous deployment                        | Streamlined, easy to grasp         | Lacks versioned releases, not for all scenarios            |
| Gitlab Flow        | Similar to GitHub Flow but uses multiple branches| Streamlined CI/CD, frequent releases                     | Easy to understand, automated CI/CD | Not suitable for all types of projects                        |
| Centralized       | Single branch, everything is developed there   | Simple, suitable for small teams or projects             | Minimal overhead, no branch clutter | Not suitable for collaborative or complex projects          |
