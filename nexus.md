# Nexus telepites

## Running Nexus Using a Docker Container
1. pull
   ```bash
   docker pull sonatype/nexus3
   ```

2. run container
   ```bash
   docker volume create nexus-data
   docker run -d -p 8081:8081 -p 8082:8082 -p 8083:8083 --name nexus -v nexus-data:/nexus-data sonatype/nexus3
   ```

3. get admin password
    ```bash
    docker exec -it nexus cat /nexus-data/admin.password
    ```


### Setting Up a Private PyPI Repository

#### 1. Create a PyPI Repository in Nexus:

   - Log in to the Nexus web interface.
   - Navigate to "Repositories" and click "Create Repository."
   - Choose the "PyPI (proxy)" repository format.
   - Configure the repository settings, such as name and storage location.
   - Set the "Remote Storage" URL to point to the public PyPI repository (https://pypi.org/simple).

#### 2. Proxy the Public PyPI Repository:

   - In the Nexus repository settings, enable the "Proxy" option.
   - Configure repository routing to proxy the public PyPI repository.

#### 3. Configure Your Python Environment:

   - Update your `pip` configuration to use your private PyPI repository as a trusted source. Create or modify the `~/.pip/pip.conf` file:

   `%APPDATA%\pip\pip.ini`

   ```conf
   [global]
   index = http://localhost:8081/repository/training360/pypi
   index-url = http://localhost:8081/repository/training360/simple
   trusted-host = localhost
   ```

   Replace `your-pypi-repo` with the name of your Nexus PyPI repository.

#### 4. Upload packages
Configure twine:
`C:\Users\USERNAME\.pypirc`

```
[distutils]
index-servers = pypi
[pypi]
repository: http://localhost:8081/repository/training360-pypi-private/
username: admin
password: Almafa123!
```

Then install with 
`twine upload dist/*`

### Setting Up a Docker Registry

Nexus can also be used as a Docker registry to store and distribute Docker images:

#### 1. Create a Docker (Docker Hosted) Repository in Nexus:

   - Log in to the Nexus web interface.
   - Navigate to "Repositories" and click "Create Repository."
   - Choose the "Docker (Docker Hosted)" repository format.
   - Configure the repository settings, such as name and storage location.

#### 2. Push Docker Images to Nexus:

   - Authenticate with your Nexus Docker repository:

   ```bash
   docker login -u <username> -p <password> localhost:8081
   ```

   Replace `<username>` and `<password>` with your Nexus credentials.

   - Build your Docker image (e.g., `docker build -t my-app .`).

   - Tag the image with your Nexus repository URL:

   ```bash
   docker tag my-app localhost:8081/repository/docker-repo/my-app:1.0
   ```

   Replace `docker-repo` with the name of your Docker repository and `my-app:1.0` with your image name and version.

   - Push the Docker image to Nexus:

   ```bash
   docker push localhost:8081/repository/docker-repo/my-app:1.0
   ```

Now, you have set up a Docker registry in Nexus and can store and retrieve Docker images as needed.