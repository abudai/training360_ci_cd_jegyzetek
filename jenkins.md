# Jenkins

[Jenkins hivatalos oldal](https://www.jenkins.io/doc/tutorials/build-a-python-app-with-pyinstaller/)

1. Image letoltese

```bash
docker pull jenkins/jenkins:lts
```
   
2. Docker volume

```bash
docker volume create jenkins_home
```

3. Jenkins futtatas

```bash
docker run -d -p 8080:8080 -p 50000:50000 --name jenkins-local -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
```

4. Admin jelszo

```bash
docker exec -it jenkins-local cat /var/jenkins_home/secrets/initialAdminPassword
```

5. Kovesd a lepeseket a localhost:8080-n

## Jenkins job types

| Job Type                  | Description                                                 | Typical Use Cases                                             |
|---------------------------|-------------------------------------------------------------|---------------------------------------------------------------|
| Freestyle Project         | Basic job type with configurable build steps and triggers | Simple build and deployment tasks                              |
| Pipeline Project          | Defines build and deployment processes as code            | Complex, version-controlled CI/CD workflows                   |
| Multi-Configuration Project | Handles multiple configurations for a single job         | Building on multiple platforms or with different parameters   |
| Matrix Project            | Executes the same build on multiple axes (configurations) | Testing on different OS versions or with various parameters  |
| GitHub Organization       | Automatically creates Pipeline jobs for GitHub org repos  | Automated CI/CD for multiple repositories in a GitHub org     |
| Parameterized Job         | Accepts user-defined parameters for customization         | Customized build and deployment based on user inputs          |
| Maven Project             | Specifically tailored for Maven-based projects           | Building, testing, and deploying Maven-based applications     |

## Pluginok telepitse:

http://localhost:8080/manage/pluginManager/

## Jenkins gitlab integracio

1. Gitlab plugion telepitese
2. Manage jenkinse
3. Gitlab PTA letrehozasa
4. Gitlab credentials hozzaadasa
5. uj job letrehozasa
6. gitlab webhooks


Jenkinsfile
```commandline
pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'python:2-alpine'
                }
            }
            steps {
                sh 'python -m py_compile src/character_count.py'
            }
        }
        stage('Test') {
            agent {
                docker {
                    image 'qnib/pytest'
                }
            }
            steps {
                sh 'py.test --verbose --junit-xml test-reports/results.xml src/test_character_count.py'
            }
            post {
                always {
                    junit 'test-reports/results.xml'
                }
            }
        }
        stage('Deliver') {
            agent {
                docker {
                    image 'cdrx/pyinstaller-linux:python2'
                }
            }
            steps {
                sh 'pyinstaller --onefile src/character_count.py'
            }
            post {
                success {
                    archiveArtifacts 'src/character_count'
                }
            }
        }
    }
}
```

